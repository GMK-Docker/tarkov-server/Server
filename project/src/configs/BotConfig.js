"use strict";

module.exports = {
    "presetBatch": {
        "assault": 120,
        "bossBully": 1,
        "bossGluhar": 1,
        "bossKilla": 1,
        "bossKojaniy": 1,
        "bossSanitar": 1,
        "bossTagilla": 1,
        "bossTest": 40,
        "cursedAssault": 120,
        "followerBully": 4,
        "followerGluharAssault": 2,
        "followerGluharScout": 2,
        "followerGluharSecurity": 2,
        "followerGluharSnipe": 2,
        "followerKojaniy": 2,
        "followerSanitar": 2,
        "followerTagilla": 2,
        "followerTest": 4,
        "marksman": 30,
        "pmcBot": 120,
        "sectantPriest": 1,
        "sectantWarrior": 5,
        "gifter": 1,
        "test": 40,
        "exUsec": 15
    },
    "bosses": ["bossbully", "bossgluhar", "bosskilla", "bosskojaniy", "bosssanitar", "bosstagilla"],
    "durability":{
        "pmcbot":{
            "armor": {
                "minPercent": 80
            },
            "weapon": {
                "minPercent": 80
            }
        },
        "exusec":{
            "armor": {
                "minPercent": 80
            },
            "weapon": {
                "minPercent": 80
            }
        },
        "pmc": {
            "armor": {
                "minPercent": 80
            },
            "weapon": {
                "minPercent": 80
            }
        },
        "boss": {
            "armor": {
                "minPercent": 80
            },
            "weapon": {
                "minPercent": 80
            }
        }
    },
    "pmc": {
        "dynamicLoot": {
            "whitelist": [
                ItemHelper.BASECLASS.Jewelry,
                ItemHelper.BASECLASS.Electronics,
                ItemHelper.BASECLASS.BuildingMaterial,
                ItemHelper.BASECLASS.Tool,
                ItemHelper.BASECLASS.HouseholdGoods,
                ItemHelper.BASECLASS.MedicalSupplies,
                ItemHelper.BASECLASS.Lubricant,
                ItemHelper.BASECLASS.Battery,
                ItemHelper.BASECLASS.Keycard,
                ItemHelper.BASECLASS.KeyMechanical,
                ItemHelper.BASECLASS.AssaultScope,
                ItemHelper.BASECLASS.ReflexSight,
                ItemHelper.BASECLASS.TacticalCombo,
                ItemHelper.BASECLASS.Magazine,
                ItemHelper.BASECLASS.Knife,
                ItemHelper.BASECLASS.BarterItem,
                ItemHelper.BASECLASS.Silencer,
                ItemHelper.BASECLASS.Foregrip,
                ItemHelper.BASECLASS.Info,
                ItemHelper.BASECLASS.Food,
                ItemHelper.BASECLASS.Drink,
                ItemHelper.BASECLASS.Drugs,
                ItemHelper.BASECLASS.Armor,
                ItemHelper.BASECLASS.Stimulator,
                ItemHelper.BASECLASS.AmmoBox,
                ItemHelper.BASECLASS.Money,
                ItemHelper.BASECLASS.Other
            ],
            "blacklist": [
                "5fca13ca637ee0341a484f46", // SJ9 TGLabs combat stimulant injector (Thermal Stim)
                "59f32c3b86f77472a31742f0", // usec dogtag
                "59f32bb586f774757e1e8442", // bear dogtag
                "6087e570b998180e9f76dc24" // Superfors DB 2020 Dead Blow Hammer
            ],
            "spawnLimits": {
                "5c99f98d86f7745c314214b3": 1, // mechanical key
                "5c164d2286f774194c5e69fa": 1, // keycard
                "550aa4cd4bdc2dd8348b456c": 2, // silencer
                "55818add4bdc2d5b648b456f": 1, // assault scope
                "55818ad54bdc2ddc698b4569": 1, // reflex sight
                "55818af64bdc2d5b648b4570": 1, // foregrip
                "5448e54d4bdc2dcc718b4568": 1, // armor
                "5448f3a64bdc2d60728b456a": 2, // stims
                "5447e1d04bdc2dff2f8b4567": 1, // knife
                "5a341c4686f77469e155819e": 1, // face cover
                "55818b164bdc2ddc698b456c": 2, // tactical laser/light
                "5448bc234bdc2d3c308b4569": 2, // Magazine
                "543be5dd4bdc2deb348b4569": 2, // Money
                "543be5cb4bdc2deb348b4568": 2 // AmmoBox
            },
            "moneyStackLimits": {
                "5449016a4bdc2d6f028b456f": 4000, // Rouble
                "5696686a4bdc2da3298b456a": 50, // USD
                "569668774bdc2da2298b4568": 50, // Euro
            }
        },
        "difficulty": "AsOnline",
        "isUsec": 50,
        "chanceSameSideIsHostilePercent": 50,
        "usecType": "bosstest",
        "bearType": "test",
        "maxLootTotalRub": 150000,
        "types": {
            "assault": 35,
            "cursedAssault": 35,
            "pmcBot": 35
        }
    },
    "showTypeInNickname": false,
    "maxBotCap": 20
};