"use strict";

module.exports = {
    "insuranceMultiplier":{
        "54cb50c76803fa8b248b4571": 0.16, // Prapor
        "54cb57776803fa99248b456e": 0.25  // Therapist
    },
    "returnChance": 80,
    "runInterval": 600
};